package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;
@Profile("MyDao")
@Repository
@Slf4j
public class StudentGavinImpl implements StudentDao{
    List<Student> students;
    public StudentGavinImpl(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1l)
                .studentId("592115521")
                .name("Zihao")
                .surname("Yu")
                .gpa(3.59)
                .image("https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjJ_pG_7aDlAhXDto8KHWrxDa0QjRx6BAgBEAQ&url=https%3A%2F%2Fwww.cosmopolitan.com%2Ftw%2Fentertainment%2Fcelebrity-gossip%2Fg28383680%2Feddie-peng-toy-shop%2F&psig=AOvVaw0dJQT0vstl1_dNmRKK9Dow&ust=1571317905036541")
                .penAmount(15)
                .description("Handsome!!!!")
                .build());

    }

    @Override
    public List<Student> getAllStudent() {
        log.info("My dao is called");
        return students;
    }

    @Override
    public Student findById(Long id) {
        return students.get((int) (id -1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }
}

